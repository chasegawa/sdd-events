function addDiverToShootoutTable($table, diver, position) {
  var rowid = "tr" + diverName;
  $table.append($('<tr id="' + rowid + '">').append($('<td>').text(position)).append($('<td>').text(diver.name)).append($('<td class="score">').text("0.0")));
}

function addPerformedDiveToDiver(diver, divename) {
  var performedDives = diver["performedDives"];
  if (performedDives == "") {
    diver["performedDives"] = divename;
  }
  else {
    if (performedDives.indexOf(divename) == -1) {
      diver["performedDives"] = performedDives + ", " + divename;
    }
  }
  localStorage.setItem(diver.name, JSON.stringify(diver));
}

function advanceShootout() {
  $('#shootoutegLeaderboard tr:last').remove();
  $('#shootoutsbLeaderboard tr:last').remove();
  $("#shootoutList").html("");
  //Build the new diver order list
  var rows = $("#shootoutegLeaderboard tbody  tr").get();
  $.each(rows, function(index, row) {
    if (index > 5) { return; }
    var name = $(row).children('td:nth-child(2)').text();   
    $("#shootoutList").prepend($('<option></option>').val(name).html(name));
    $(row).children('td:nth-child(3)').text("0.0");  
  });
  rows = $("#shootoutsbLeaderboard tbody  tr").get();
  $.each(rows, function(index, row) {
    if (index > 5) { return; }
    var name = $(row).children('td:nth-child(2)').text();   
    $("#shootoutList").prepend($('<option></option>').val(name).html(name));
    $(row).children('td:nth-child(3)').text("0.0");
  });
  //Add the first diver up
  var currentDiver = $("#shootoutList option:first").val();
  localStorage.setItem("currentDiver", currentDiver);
  setCurrentShootoutDiver();
  $("#sNextRound").hide();
  
}

function clearSSDive() {
  $("#sdive").val("");
  $("#sheight").val("1m");
  $("#ss1").val("");
  $("#ss2").val("");
  $("#ss3").val("");
  $("#sdesc").text("");
  $("#sdiveDD").text("");
}

function initializeShootout() {
  $('#accordion').accordion("option", "active", 2);
  $("#shootout").show();
  // Build the diver order list
  var rows = $("#group1Leaderboard tbody  tr").get();
  $.each(rows, function(index, row) {
    if (index > 5) { return; }
    var name = $(row).children('td:nth-child(2)').text();   
    $("#shootoutList").prepend($('<option></option>').val(name).html(name));
    diver = JSON.parse(localStorage.getItem(name));
    diver["performedDives"] = "";
    diver["lastDive"] = "";
    localStorage.setItem(diver.name, JSON.stringify(diver));
    addDiverToShootoutTable($("#shootoutegLeaderboard").find('tbody'), diver, index+1);
  });
  rows = $("#group2Leaderboard tbody  tr").get();
  $.each(rows, function(index, row) {
    if (index > 5) { return; }
    var name = $(row).children('td:nth-child(2)').text();   
    $("#shootoutList").prepend($('<option></option>').val(name).html(name));
    diver = JSON.parse(localStorage.getItem(name));
    diver["performedDives"] = "";
    diver["lastDive"] = "";
    localStorage.setItem(diver.name, JSON.stringify(diver));
    addDiverToShootoutTable($("#shootoutsbLeaderboard").find('tbody'), diver, index+1);
  });
  // Add the first diver up
  var currentDiver = $("#shootoutList option:first").val();
  localStorage.setItem("currentDiver", currentDiver);
  setCurrentShootoutDiver();
}

function matchTheDive() {
  var diveNum = $("#sdive").val().toUpperCase();
  $("#sdive").val(diveNum);
  var dive = diveNum + $("#sheight").val();
  var diveDetail;
  for (var i=0; i < ALL_DIVES.length; i++) {
    if (ALL_DIVES[i][0].toUpperCase() == dive.toUpperCase()) {
      diveDetail = ALL_DIVES[i];
      break;
    }
  }
  if (diveDetail != null) {
    $("#sdiveDD").text(diveDetail[2]);
    $("#sdesc").text(diveDetail[1]);
  }
  else {
    alert("Dive number and board not a valid combination");
  }
}

function scoreCurrentDiver() {
  var ddVal = Number($("#sdiveDD").text());
  var diverScore = (ddVal * (Number($("#ss1").val()) + Number($("#ss2").val()) + Number($("#ss3").val()))).toFixed(2);
  var diverName = localStorage.getItem("currentDiver");
  var currentDiver = JSON.parse(localStorage.getItem(diverName)); 
  addPerformedDiveToDiver(currentDiver, $("#sdive").val());
  currentDiver["ss1"] = $("#ss1").val();
  currentDiver["ss2"] = $("#ss2").val();
  currentDiver["ss3"] = $("#ss3").val();
  currentDiver["lastDive"] = $("#sdive").val();
  if (currentDiver.group == "1") {
    updateShootoutDiverScore("shootoutegLeaderboard", currentDiver, diverScore);
    sortTable("shootoutegLeaderboard");
  } else {
    updateShootoutDiverScore("shootoutsbLeaderboard", currentDiver, diverScore);
    sortTable("shootoutsbLeaderboard");
  }
  localStorage.setItem(diverName, JSON.stringify(currentDiver));
}

function setCurrentShootoutDiver() {
  var diverName = localStorage.getItem("currentDiver");
  var diver = JSON.parse(localStorage.getItem(diverName)); 
  $("#shootoutDiverName").text(diverName);
  $("#shootoutList").val(diverName);
  $("#previousDives").text(diver["performedDives"]);
  $("#ss1").val(diver["ss1"]);
  $("#ss2").val(diver["ss2"]);
  $("#ss3").val(diver["ss3"]);
  $("#sdive").val(diver["lastDive"]);
}

function shootoutNextDiver() {
  scoreCurrentDiver();
  clearSSDive();
  // Check if end of the round
  var currentDiverIndex = $("#shootoutList")[0].selectedIndex;
  var numItems = $("#shootoutList")[0].length;
  if (numItems == (currentDiverIndex+1)) {
    $("#sNextRound").show();
  }
  else {
    $("#shootoutList")[0].selectedIndex = currentDiverIndex + 1;
    var currentDiver = $("#shootoutList option:selected").val();
    localStorage.setItem("currentDiver", currentDiver);
    setCurrentShootoutDiver();
  }
}

function shootoutPreviousDiver() {
  clearSSDive();
  var currentDiverIndex = $("#shootoutList")[0].selectedIndex;
  var numItems = $("#shootoutList")[0].length;
  if (currentDiverIndex == 0) {
    setCurrentShootoutDiver();
    matchTheDive();
    return;
  }
  else {
    $("#shootoutList")[0].selectedIndex = currentDiverIndex - 1;
    var currentDiver = $("#shootoutList option:selected").val();
    localStorage.setItem("currentDiver", currentDiver);
    setCurrentShootoutDiver();
    matchTheDive();
  }
}

function updateShootoutDiverScore(tableName, diver, score) {
  var rows = $("#" + tableName + " tbody  tr").get();
  $.each(rows, function(index, row) {
    if (diver.name == $(row).children('td:nth-child(2)').text()) {
      $(row).children('td:nth-child(3)').text(score);
    }
  });
}