/*
 * Add a row to the leaderboard table. Give the diver the position supplied (for display - the row is simply appended to the table).
 */
function addDiverToTable($table, diver, position) {
  var rowid = "tr" + diverName;
  $table.append($('<tr id="' + rowid + '">').append($('<td>').text(position)).append($('<td>').text(diver.name)).append($('<td class="score">').text(totalDiverRoundScores(diver))));
}

/*
 * Find the DD of a given dive
 */
function getDD(dive) {
  if (typeof dive == 'undefined') { return ""; }
  var diveDetail;
  for (var i=0; i < ALL_DIVES.length; i++) {
    if (ALL_DIVES[i][0].toUpperCase() == dive.toUpperCase()) {
      diveDetail = ALL_DIVES[i];
      break;
    }
  }
  return diveDetail[2];
}

function quickFormatScore(scoreField) {
  var score = $(scoreField).val();
  if (score == 'undefined' || score == '') {return;}
  var numScore = Number(score);
  if (numScore > 10) {
    numScore = numScore / 10
  }
  $(scoreField).val(numScore.toFixed(1));
}

/*
 * Simple function to shuffle an array of values
 */
function shuffle(array) {
  for (var i = array.length - 1; i > 0; i--) {
      var j = Math.floor(Math.random() * (i + 1));
      var temp = array[i];
      array[i] = array[j];
      array[j] = temp;
  }
  return array;
}

/*
 * Sort the table using the third column
 */
function sortTable(tableName) {
  var rows = $("#" + tableName + " tbody  tr").get();
  rows.sort(function(a, b) {
    var A = Number($(a).children('td').eq(2).text());
    var B = Number($(b).children('td').eq(2).text());
    if(B < A) {
      return -1;
    }
    if(B > A) {
      return 1;
    }
    return 0;
  });

  $.each(rows, function(index, row) {
    $(row).children('td:first').text(index+1);
    $("#"+tableName).children('tbody').append(row);
  });
}

/*
 * Computes the total of all the diver's scores
 */
function totalDiverRoundScores(diver) {
  var total = 0.0;
  if (diver == null) { return total.toFixed(2); }
  for (var i=1; i < 6; i++) {
    try {
      total += Number(diver["roundScores"][i-1]);
    }
    catch (err) { break ; } // As soon as there are not scores, we are done.
  }
  var hack = Number(diver["roundScores"][5]);
  if (!isNaN(hack)) { total+=hack;} // Had to add this because I hosed up the clear logic during a meet
  return total.toFixed(2);
}

/*
 * Updates the diver's score on the leaderboard that they are located on.
 */
function updateDiverScore(tableName, diver) {
  var rows = $("#" + tableName + " tbody  tr").get();
  $.each(rows, function(index, row) {
    if (diver.name == $(row).children('td:nth-child(2)').text()) {
      $(row).children('td:nth-child(3)').text(totalDiverRoundScores(diver));
    }
  });
}