/*
 * Clear the diver detail from the registration fields
 */
function clearDiverRegistration() {
  $("#diverName").val("");
  $("#diverGroup").val("");
  $("#dive1").val("");
  $("#dive2").val("");
  $("#dive3").val("");
  $("#dive4").val("");
  $("#dive5").val("");
  $("#height1").val("1m");
  $("#height2").val("1m");
  $("#height3").val("1m");
  $("#height4").val("1m");
  $("#height5").val("1m");
  $("#dive1DD").text("");
  $("#dive2DD").text("");
  $("#dive3DD").text("");
  $("#dive4DD").text("");
  $("#dive5DD").text("");
  $("#desc1").text("");
  $("#desc2").text("");
  $("#desc3").text("");
  $("#desc4").text("");
  $("#desc5").text("");
  $("#groupDisplay").text("");
  $("#saveNewDiver").hide();
}

/*
 * Find and display the matching dive in the registration area
 */
function matchDive(listNum) {
  var diveNum = $("#dive" + listNum).val().toUpperCase();
  $("#dive" + listNum).val(diveNum);
  var dive = diveNum + $("#height" + listNum).val();
  var diveDetail;
  for (var i=0; i < ALL_DIVES.length; i++) {
    if (ALL_DIVES[i][0].toUpperCase() == dive.toUpperCase()) {
      diveDetail = ALL_DIVES[i];
      break;
    }
  }
  if (diveDetail != null) {
    $("#dive"+listNum+"DD").text(diveDetail[2]);
    $("#desc"+listNum).text(diveDetail[1]);
  }
  else {
    alert("Dive number and board not a valid combination");
  }
}

/*
 * Move the diver to the target group
 */
function moveDiverToGroup(targetGroup, diverName) {
  var originalGroup = (targetGroup == 1) ? 2 : 1;
  var diver = JSON.parse(localStorage.getItem(diverName));
  diver.group = targetGroup;
  localStorage.setItem(diver.name, JSON.stringify(diver));
  $("#diverList" + originalGroup).find(":selected").remove();
  $("#diverList" + targetGroup).append($('<option></option>').val(diver.name).html(diver.name));
}

/*
 * Save the registration information for the diver
 */
function saveDiverDetail() {
  var diver = {};
  diver.name = $("#diverName").val();
  diver.group = $("#diverGroup").val();
  diver.dive1 = $("#dive1").val();
  diver.dive2 = $("#dive2").val();
  diver.dive3 = $("#dive3").val();
  diver.dive4 = $("#dive4").val();
  diver.dive5 = $("#dive5").val();
  diver.height1 = $("#height1").val();
  diver.height2 = $("#height2").val();
  diver.height3 = $("#height3").val();
  diver.height4 = $("#height4").val();
  diver.height5 = $("#height5").val();
  diver.score1 = ["","",""];
  diver.score2 = ["","",""];
  diver.score3 = ["","",""];
  diver.score4 = ["","",""];
  diver.score5 = ["","",""];
  diver.roundScores = [0.0, 0.0, 0.0, 0.0, 0.0];
  diver.total = 0.0;
  diver.performedDives = "";
  // Put the diver detail into local storage keyed by name
  localStorage.setItem(diver.name, JSON.stringify(diver));
  // Check the list of divers. If this is a new diver, update the list of divers in local storage, otherwise this is an
  // update to an already registered diver
  if (!($.inArray(diver.name, diverNames) > -1)) { 
    diverNames.push(diver.name);
    localStorage.setItem("diverNames", JSON.stringify(diverNames));
    $("#diverList" + diver.group).append($('<option></option>').val(diver.name).html(diver.name));
  }
}

/*
 * Show the selected diver in the registration detail
 */
function showDiver(listNum) {
  var diverName = $("#diverList" + listNum).val();
  if (diverName == null) {return;}
  var diver = JSON.parse(localStorage.getItem(diverName));
  $("#diverGroup").val(diver.group);
  $("#diverName").val(diver.name);
  $("#dive1").val(diver.dive1);
  $("#dive2").val(diver.dive2);
  $("#dive3").val(diver.dive3);
  $("#dive4").val(diver.dive4);
  $("#dive5").val(diver.dive5);
  $("#height1").val(diver.height1);
  $("#height2").val(diver.height2);
  $("#height3").val(diver.height3);
  $("#height4").val(diver.height4);
  $("#height5").val(diver.height5);
  matchDive(1);
  matchDive(2);
  matchDive(3);
  matchDive(4);
  matchDive(5);
  $("#groupDisplay").text("Group " + diver.group);
  $("#saveNewDiver").show();
}

/*
 * Remove the diver from the registration list
 */
function deleteDiver(groupNum) {
  var diverName = $("#diverList" + groupNum).val();
  $("#diverList" + groupNum).find(":selected").remove();
  var index = diverNames.indexOf(diverName);
  diverNames.splice(index, 1);
  localStorage.setItem("divers", JSON.stringify(diverNames));
  localStorage.removeItem(diverName);
  clearDiverRegistration();
}

function loadDefaultDivers() {
  var g1 = ["Keller Adair", "Chris Coburn", "Derek Bowers", "Warren McElroy", "Serena Sedillo", "Rachael Burton", "Alexis Tupponce", "Skylar Williams", "Grace Brouse", "Molly Vedder", "Sam Stein", "Kira Hansen", "Jason Lenzo", "Ashton Hasegawa" ]; 
  var g2 = ["Henry Bentson", "Zach Lundgren", "Abby Schmitz", "Gavin Buttram", "Mila Glass", "Brooklyn Hanania", "Riley Hananai", "Elisabet Olsson", "Tessa Sharp", "Christo Tzeremas", "Michael Tzeremas", "Grace Butler", "Rachel Lemons", "McKenzie Wagner", "Ainsley Hasegawa", "Matt Lenzo", "Alexandra Figueroa"];
  for (var i = 0; i < g1.length; i++) {
	  var diver = {};
	  diver.name = g1[i];
	  diver.group = "1";
	  diver.dive1 = "101C";
	  diver.dive2 = "201C";
	  diver.dive3 = "301C";
	  diver.dive4 = "401C";
	  diver.dive5 = "5122D";
	  diver.height1 = "1m";
	  diver.height2 = "1m";
	  diver.height3 = "1m";
	  diver.height4 = "1m";
	  diver.height5 = "1m";
	  diver.score1 = ["","",""];
	  diver.score2 = ["","",""];
	  diver.score3 = ["","",""];
	  diver.score4 = ["","",""];
	  diver.score5 = ["","",""];
	  diver.roundScores = [0.0, 0.0, 0.0, 0.0, 0.0];
	  diver.total = 0.0;
	  diver.performedDives = "";
	  // Put the diver detail into local storage keyed by name
	  localStorage.setItem(diver.name, JSON.stringify(diver));
	  // Check the list of divers. If this is a new diver, update the list of divers in local storage, otherwise this is an
	  // update to an already registered diver
	  if (!($.inArray(diver.name, diverNames) > -1)) { 
	    diverNames.push(diver.name);
	    localStorage.setItem("diverNames", JSON.stringify(diverNames));
	  }
  }
  for (var i = 0; i < g2.length; i++) {
	  var diver = {};
	  diver.name = g2[i];
	  diver.group = "2";
	  diver.dive1 = "101C";
	  diver.dive2 = "201C";
	  diver.dive3 = "301C";
	  diver.dive4 = "401C";
	  diver.dive5 = "5301D";
	  diver.height1 = "1m";
	  diver.height2 = "1m";
	  diver.height3 = "1m";
	  diver.height4 = "1m";
	  diver.height5 = "1m";
	  diver.score1 = ["","",""];
	  diver.score2 = ["","",""];
	  diver.score3 = ["","",""];
	  diver.score4 = ["","",""];
	  diver.score5 = ["","",""];
	  diver.roundScores = [0.0, 0.0, 0.0, 0.0, 0.0];
	  diver.total = 0.0;
	  diver.performedDives = "";
	  // Put the diver detail into local storage keyed by name
	  localStorage.setItem(diver.name, JSON.stringify(diver));
	  // Check the list of divers. If this is a new diver, update the list of divers in local storage, otherwise this is an
	  // update to an already registered diver
	  if (!($.inArray(diver.name, diverNames) > -1)) { 
	    diverNames.push(diver.name);
	    localStorage.setItem("diverNames", JSON.stringify(diverNames));
	  }
  }
}