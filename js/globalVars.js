// The array of diver names
var diverNames = [];

//Local var to hold which round of the qualifying we are currently in
var currentQualifyingRound = 1;

//"Warren McElroy"
//1: "Derek Bowers"
//2: "Jasmine Rogers"
//3: "Ashton Hasegawa"
//4: "Alexis Tupponce"
//5: "Rachel Lemons"
//6: "Samantha Stein"
//7: "Lexi Spigelman"
//8: "Jason Lenzo"
//9: "Serena Sedillo"
//10: "Siobhan Ferrall"
//11: "Ainsley Hasegawa"
//12: "Rachel Lawful"
//13: "Raquel Gonzales"
//14: "Mads Russell"
//15: "Megan Bowers"
//16: "Blake Disney"
//17: "Jordon Nelson"
//18: "Emmott Blitch"
//19: "Morgan Harder"
//20: "Henry Bentson"
//21: "Abby Schmitz"
//22: "Matt Lenzo"
//23: "Skylar Williams"