/*
 * Change the current round by the supplied amount. Cannot go back past round 1
 */
function changeCurrentRound(changeValue) {
  currentQualifyingRound = Number(currentQualifyingRound) + Number(changeValue);
  if (isNaN(currentQualifyingRound) || currentQualifyingRound < 1) { currentQualifyingRound = 1; }
  localStorage.setItem("currentQualifyingRound", currentQualifyingRound);
  $("#roundNum").text(currentQualifyingRound);
}

/*
 * Initialize the qualifying leaderboards
 */
function initializeQualifyingLeaderboards(){
  var diver;
  var position = 1;
  $("#diverList1 > option").each(function() {
    diver = JSON.parse(localStorage.getItem($(this).val()));
    addDiverToTable($("#group1Leaderboard").find('tbody'), diver, position++);
  });
  sortTable("group1Leaderboard");
  position = 1;
  $("#diverList2 > option").each(function() {
    diver = JSON.parse(localStorage.getItem($(this).val()));
    addDiverToTable($("#group2Leaderboard").find('tbody'), diver, position++);
  });
  sortTable("group2Leaderboard");
}

/*
 * Advance to the next diver in the qualifying competition
 */
function nextDiver() {
  saveCurrentDiverDetail();
  // move to the next diver in the list
  var currentDiverIndex = $("#qualifyingList")[0].selectedIndex;
  var numItems = $("#qualifyingList")[0].length;
  // If it is the end of the list, advance the round and then start at the top
  if (numItems == (currentDiverIndex+1)) {
    changeCurrentRound(1);
    // If we turned to round 6, we are done, so do nothing more
    if (currentQualifyingRound > 5) {
      currentQualifyingRound = 5; 
      localStorage.setItem("currentQualifyingRound", currentQualifyingRound);
      $("#roundNum").text(currentQualifyingRound);
      return;
    }
    $("#roundNum").text(currentQualifyingRound);
    $("#qualifyingList")[0].selectedIndex = 0;
    var currentDiver = $("#qualifyingList option:selected").val();
    localStorage.setItem("currentQualifyingDiver", currentDiver);
    showCurrentDiver();
    return;
  }
  // otherwise, select the next diver in the list as selected and update "currentDiver" in localStorage
  $("#qualifyingList")[0].selectedIndex = currentDiverIndex + 1;
  var currentDiver = $("#qualifyingList option:selected").val();
  localStorage.setItem("currentQualifyingDiver", currentDiver);
  showCurrentDiver();
}

/*
 * Go to the previous diver in the qualifying round.
 */
function previousDiver() {
  saveCurrentDiverDetail();
  var currentDiverIndex = $("#qualifyingList")[0].selectedIndex;
  // If it is the start of the list, reduce the round and then start at the bottom
  if (0 == currentDiverIndex) {
    // Can't previous if we are in round 1 and on the first diver
    if (currentQualifyingRound == 1) { return; }
    changeCurrentRound(-1);
    $("#roundNum").text(currentQualifyingRound);
    // Set to last
    $("#qualifyingList")[0].selectedIndex = ($("#qualifyingList")[0].length - 1);
    var currentDiver = $("#qualifyingList option:selected").val();
    localStorage.setItem("currentQualifyingDiver", currentDiver);
    showCurrentDiver();
    return;
  }
  // otherwise, select the previous diver in the list as selected and update "currentDiver" in localStorage
  $("#qualifyingList")[0].selectedIndex = currentDiverIndex - 1;
  var currentDiver = $("#qualifyingList option:selected").val();
  localStorage.setItem("currentQualifyingDiver", currentDiver);
  showCurrentDiver();
}

/*
 * Browser was reloaded - reload existing qualifying state
 */
function reloadQualifying() {  
  $("#start").hide();
  var currentRound = localStorage.getItem("currentQualifyingRound");
  if (currentRound == 'undefined' || isNaN(currentRound)) {
    localStorage.setItem("currentQualifyingRound", 1);
  }
  showDiversForQualifying();
  var currentDiver = localStorage.getItem("currentQualifyingDiver");
  $("#qualifyingList").val(currentDiver);
  $('#accordion').accordion("option", "active", 1);
  $("#roundNum").text(currentRound);
  showCurrentDiver();
  initializeQualifyingLeaderboards();
  $("#qualifying").show();
}

/*
 * Save the current diver in the qualifying competition. This will:
 * - Update the diver's information with the scores entered
 * - Update their total score
 * - Update the leaderboard that the diver belongs on. 
 */
function saveCurrentDiverDetail() {
  var diverName = localStorage.getItem("currentQualifyingDiver");
  var currentDiver = JSON.parse(localStorage.getItem(diverName));
  currentDiver["score"+currentQualifyingRound] = [$("#score1").val(), $("#score2").val(), $("#score3").val()];
  currentDiver["note"+currentQualifyingRound] = $("#note").val();
  var roundScore = (Number($("#score1").val()) + Number($("#score2").val()) + Number($("#score3").val())) * Number($("#dd").val());
  roundScore = roundScore.toFixed(2);
  currentDiver["roundScores"][currentQualifyingRound] = roundScore;
  // Update the current diver in storage
  localStorage.setItem(currentDiver.name, JSON.stringify(currentDiver));
  if (currentDiver.group == DIVE_GROUP1) {
    updateDiverScore("group1Leaderboard", currentDiver);
    sortTable("group1Leaderboard");
  } else {
    updateDiverScore("group2Leaderboard", currentDiver);
    sortTable("group2Leaderboard");
  }
}

/*
 * User has pressed the start button to run qualifying. 
 */
function setupQualifying() {
  $("#start").hide();  
  localStorage.setItem("currentQualifyingRound", 1);
  shuffle(diverNames);
  // Add a local storage value to show we started qualifying
  localStorage.setItem("qualifyingDivers", JSON.stringify(diverNames));
  changeCurrentRound(-10);
  showDiversForQualifying();
  // select the first diver to start things
  var diverName = $("#qualifyingList option:first").val();
  localStorage.setItem("currentQualifyingDiver", diverName);
  $("#qualifyingList").val(diverName);
  showCurrentDiver();
  initializeQualifyingLeaderboards();
  $("#qualifying").show();
}

/*
 * Set/show the current diver in the qualifying competition
 */
function showCurrentDiver() {
  var diverName = localStorage.getItem("currentQualifyingDiver");
  var currentDiver = JSON.parse(localStorage.getItem(diverName));
  var currentRound = localStorage.getItem("currentQualifyingRound");
  $("#currentDiverName").text(diverName);
  var dd = getDD(currentDiver["dive"+currentRound] + currentDiver["height"+currentRound]);
  $("#dd").val(dd);
  var dive = "[" + currentDiver["height"+currentRound] + "] " + currentDiver["dive"+currentRound] + " / DD: " + dd;
  $("#currentDiverDive").text(dive);
  var scores = currentDiver["score"+currentRound];
  if (typeof scores == 'undefined') { scores = ['','','']; }
  $("#score1").val(scores[0]);
  $("#score2").val(scores[1]);
  $("#score3").val(scores[2]);
  var note = currentDiver["note"+currentRound];
  if (typeof note == 'undefined') { note = ""; }
  $("note").val(note);
}

/*
 * Show the list of divers into the qualifying list display.
 */
function showDiversForQualifying() {
  for (var i=0; i < diverNames.length; i++) {
    var diver = JSON.parse(localStorage.getItem(diverNames[i]));
    $("#qualifyingList").append($('<option></option>').val(diver.name).html(diver.name));
  }
}

/*
 * Reset all the shootout data
 */
function reset() {
  // Clear qualifying leaderboards
  $("#group1Leaderboard > tbody").html("");
  $("#group2Leaderboard > tbody").html("");
  // Clear divers from local
//  divers = {};
//  for (var i=0; i < diverNames.length; i++ ) {
//    localStorage.removeItem(diverNames[i]);
//  }
  localStorage.setItem("divers", []);
  // Clear divers from the qualifying list of names
  $("#qualifyingList").html("");
  //$("#diverList1").html("");
  //$("#diverList2").html("");
  
  // Clear the qualifying diver display
  $("#currentDiverName").text("");
  $("#dd").val("");
  $("#currentDiverDive").text("");
  $("#score1").val("");
  $("#score2").val("");
  $("#score3").val("");
  $("note").val("");
  
  // Remove qualifying round detail from localStorage
  localStorage.removeItem("qualifyingRound");
  localStorage.removeItem("currentDiver");
  // Hide the display - go back to initial state.
  $("#qualifying").hide();
  $("#start").show();
}