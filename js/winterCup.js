var group1BracketData;
var group1Pairings = [];
var group2BracketData;
var group2Pairings = [];
var bracketRound=0;
var currentFinalsRound=1;

/*
 * 
 */
function advanceWinterCupRound() {
  // Make sure we are saved off first
  saveCurrentFinalsDiverDetail();
  updateBracketUI();
  changeCurrentFinalsRound(-10);
  bracketRound++;
  if (bracketRound == 2) {
    $('#wcNextRound').hide();
  }
  setupDiverBracketList();
}

/*
 * Change the current round by the supplied amount. Cannot go back past round 1
 * ** This is the round of diving, not bracket round **
 */
function changeCurrentFinalsRound(changeValue) {
  currentFinalsRound = Number(currentFinalsRound) + Number(changeValue);
  if (isNaN(currentFinalsRound) || currentFinalsRound < 1) { currentFinalsRound = 1; }
  localStorage.setItem("currentFinalsRound", currentFinalsRound);
  $("#wcRoundNum").text(currentFinalsRound);
}

/*
 * Utility function to clear the registered divers' scores
 */
function clearDiverScores(diver) {
  diver["score1"] = ["","",""];
  diver["score2"] = ["","",""];
  diver["score3"] = ["","",""];
  diver["score4"] = ["","",""];
  diver["score5"] = ["","",""];
  diver["roundScores"] = [0.0, 0.0, 0.0, 0.0, 0.0];
  diver.total = 0.0;
  // Put the diver detail into local storage keyed by name
  localStorage.setItem(diver.name, JSON.stringify(diver));
}

/*
 * Using the wcDiver list, clear all the round scores from the divers
 */
function clearDiversScores() {
  $("#wcDiverList option").each(function() {
    var diver = JSON.parse(localStorage.getItem($(this).val()));
    clearDiverScores(diver);
  });
}

function initializeBrackets() {
  initializeBracketUI();
  setupDiverBracketList();
  
  // select the first diver to start things
  var diverName = $("#wcDiverList option:first").val();
  localStorage.setItem("currentFinalsDiver", diverName);
  localStorage.setItem("currentFinalsRound", currentFinalsRound);
  $("#wcDiverList").val(diverName);
  showCurrentFinalsDiver();
  $("#winterCup").show();
  $('#accordion').accordion("option", "active", 2);
}

function initializeBracketUI() {
  var diverName1 = $("#group1Leaderboard tbody tr:nth-child(1) td:nth-child(2)").text();
  var diverName2 = $("#group1Leaderboard tbody tr:nth-child(8) td:nth-child(2)").text();
  var diverPair1 = pairDivers(diverName1, diverName2);
  diverName1 = $("#group1Leaderboard tbody tr:nth-child(2) td:nth-child(2)").text();
  diverName2 = $("#group1Leaderboard tbody tr:nth-child(7) td:nth-child(2)").text();
  var diverPair2 = pairDivers(diverName1, diverName2);
  diverName1 = $("#group1Leaderboard tbody tr:nth-child(3) td:nth-child(2)").text();
  diverName2 = $("#group1Leaderboard tbody tr:nth-child(6) td:nth-child(2)").text();
  var diverPair3 = pairDivers(diverName1, diverName2);
  diverName1 = $("#group1Leaderboard tbody tr:nth-child(4) td:nth-child(2)").text();
  diverName2 = $("#group1Leaderboard tbody tr:nth-child(5) td:nth-child(2)").text();
  var diverPair4 = pairDivers(diverName1, diverName2);
    
  group1BracketData = {
    teams: [diverPair1,diverPair4,diverPair3,diverPair2],
    results: [ [ [ [], [], [] ], [ [],[] ], [ [] ] ] ]
  };
  group1Pairings.push(group1BracketData.teams);
  
  diverName1 = $("#group2Leaderboard tbody tr:nth-child(1) td:nth-child(2)").text();
  diverName2 = $("#group2Leaderboard tbody tr:nth-child(8) td:nth-child(2)").text();
  diverPair1 = pairDivers(diverName1, diverName2);
  diverName1 = $("#group2Leaderboard tbody tr:nth-child(2) td:nth-child(2)").text();
  diverName2 = $("#group2Leaderboard tbody tr:nth-child(7) td:nth-child(2)").text();
  diverPair2 = pairDivers(diverName1, diverName2);
  diverName1 = $("#group2Leaderboard tbody tr:nth-child(3) td:nth-child(2)").text();
  diverName2 = $("#group2Leaderboard tbody tr:nth-child(6) td:nth-child(2)").text();
  diverPair3 = pairDivers(diverName1, diverName2);
  diverName1 = $("#group2Leaderboard tbody tr:nth-child(4) td:nth-child(2)").text();
  diverName2 = $("#group2Leaderboard tbody tr:nth-child(5) td:nth-child(2)").text();
  diverPair4 = pairDivers(diverName1, diverName2);
  
  group2BracketData = {
      teams: [diverPair1,diverPair4,diverPair3,diverPair2],
      results: [ [ [ [], [], [] ], [ [],[] ], [ [] ] ] ]
    };
  group2Pairings.push(group2BracketData.teams);
}

/*
 * Advance to the next diver in the finals competition
 */
function nextWCDiver() {
  saveCurrentFinalsDiverDetail();
  updateBracketUI();
  // move to the next diver in the list
  var currentDiverIndex = $("#wcDiverList")[0].selectedIndex;
  var numItems = $("#wcDiverList")[0].length;
  // If it is the end of the list, advance the round and then start at the top
  if (numItems == (currentDiverIndex+1)) {
    changeCurrentFinalsRound(1);
    // If we turned to round 6, we are done, so do nothing more
    if (currentFinalsRound > 5) {
      currentFinalsRound = 5; 
      localStorage.setItem("currentFinalsRound", currentFinalsRound);
      $("#wcRoundNum").text(currentFinalsRound);
      return;
    }
    $("#wcRoundNum").text(currentFinalsRound);
    $("#wcDiverList")[0].selectedIndex = 0;
    var currentDiver = $("#wcDiverList option:selected").val();
    localStorage.setItem("currentFinalsDiver", currentDiver);
    showCurrentFinalsDiver();
    return;
  }
  // otherwise, select the next diver in the list as selected and update "currentDiver" in localStorage
  $("#wcDiverList")[0].selectedIndex = currentDiverIndex + 1;
  var currentDiver = $("#wcDiverList option:selected").val();
  localStorage.setItem("currentFinalsDiver", currentDiver);
  showCurrentFinalsDiver();
}

/*
 * If the second diver is "undefined" create a bye for the diver
 * **If there are less than 5 divers, this probably blows up.
 */
function pairDivers(diverName1, diverName2) {
  if (diverName2 == 'undefined' || diverName2 == null || diverName2 == "") {
    return [diverName1, "BYE"];
  }
  return [diverName1, diverName2];
}

/*
 * Go to the previous diver in the bracket round.
 */
function previousWCDiver() {
  saveCurrentFinalsDiverDetail();
  var currentDiverIndex = $("#wcDiverList")[0].selectedIndex;
  // If it is the start of the list, reduce the round and then start at the bottom
  if (0 == currentDiverIndex) {
    // Can't previous if we are in round 1 and on the first diver
    if (currentFinalsRound == 1) { return; }
    changeCurrentFinalsRound(-1);
    $("#wcRoundNum").text(currentFinalsRound);
    // Set to last
    $("#wcDiverList")[0].selectedIndex = ($("#wcDiverList")[0].length - 1);
    var currentDiver = $("#wcDiverList option:selected").val();
    localStorage.setItem("currentFinalsDiver", currentDiver);
    showCurrentFinalsDiver();
    return;
  }
  // otherwise, select the previous diver in the list as selected and update "currentDiver" in localStorage
  $("#wcDiverList")[0].selectedIndex = currentDiverIndex - 1;
  var currentDiver = $("#wcDiverList option:selected").val();
  localStorage.setItem("currentFinalsDiver", currentDiver);
  showCurrentFinalsDiver();
}

function reviseWCDiverList() {
  $('#wcDiverList').html(''); // Empty the list
  switch (bracketRound) {
  case 1:
    for (var i=0; i<group1Pairings[1].length; i++) {
      var diver1Name = JSON.parse(localStorage.getItem(group1Pairings[1][i][0])).name;
      var diver2Name = JSON.parse(localStorage.getItem(group1Pairings[1][i][1])).name;
      $("#wcDiverList").append($('<option></option>').val(diver1Name).html(diver1Name));
      $("#wcDiverList").append($('<option></option>').val(diver2Name).html(diver2Name));
    }
    for (var i=0; i<group2Pairings[1].length; i++) {
      var diver1Name = JSON.parse(localStorage.getItem(group2Pairings[1][i][0])).name;
      var diver2Name = JSON.parse(localStorage.getItem(group2Pairings[1][i][1])).name;
      $("#wcDiverList").append($('<option></option>').val(diver1Name).html(diver1Name));
      $("#wcDiverList").append($('<option></option>').val(diver2Name).html(diver2Name));
    }
    break;
  case 2:
    for (var i=0; i<group1Pairings[2].length; i++) {
      var diver1Name = JSON.parse(localStorage.getItem(group1Pairings[2][i][0])).name;
      var diver2Name = JSON.parse(localStorage.getItem(group1Pairings[2][i][1])).name;
      $("#wcDiverList").append($('<option></option>').val(diver1Name).html(diver1Name));
      $("#wcDiverList").append($('<option></option>').val(diver2Name).html(diver2Name));
    }
    for (var i=0; i<group2Pairings[2].length; i++) {
      var diver1Name = JSON.parse(localStorage.getItem(group2Pairings[2][i][0])).name;
      var diver2Name = JSON.parse(localStorage.getItem(group2Pairings[2][i][1])).name;
      $("#wcDiverList").append($('<option></option>').val(diver1Name).html(diver1Name));
      $("#wcDiverList").append($('<option></option>').val(diver2Name).html(diver2Name));
    }
    break;
  }
  var selectOptions = $('#wcDiverList option').sort(function(a,b) {
    var diver1 = JSON.parse(localStorage.getItem($(a).val()));
    var diver2 = JSON.parse(localStorage.getItem($(b).val()));
    return Number(totalDiverRoundScores(diver1))-Number(totalDiverRoundScores(diver2));
  });
  $("#wcDiverList").empty().append(selectOptions);
  //select the first diver to start things
  var diverName = $("#wcDiverList option:first").val();
  localStorage.setItem("currentFinalsDiver", diverName);
  $("#wcDiverList").val(diverName);
  showCurrentFinalsDiver();
}

/*
 * Save the current diver in the finals competition. This will:
 * - Update the diver's information with the scores entered
 * - Update their total score
 */
function saveCurrentFinalsDiverDetail() {
  var diverName = localStorage.getItem("currentFinalsDiver");
  var currentDiver = JSON.parse(localStorage.getItem(diverName));
  currentDiver["score"+currentFinalsRound] = [$("#wcScore1").val(), $("#wcScore2").val(), $("#wcScore3").val()];
  currentDiver["note"+currentFinalsRound] = $("#note").val();
  var roundScore = (Number($("#wcScore1").val()) + Number($("#wcScore2").val()) + Number($("#wcScore3").val())) * Number($("#wcDD").val());
  roundScore = roundScore.toFixed(2);
  currentDiver["roundScores"][currentFinalsRound] = roundScore;
  // Update the current diver in storage
  localStorage.setItem(currentDiver.name, JSON.stringify(currentDiver));
}

function setupDiverBracketList() {
  switch (bracketRound) {
  case 0:
    // Order dive list group2 reverse order, group1 reverse order.
    for (var i=8; i>0; i--) {
      var diverName = $("#group1Leaderboard tbody tr:nth-child(" + i + ") td:nth-child(2)").text();
      if (diverName == 'undefined' || diverName == null || diverName == "") { continue; }
      $("#wcDiverList").append($('<option></option>').val(diverName).html(diverName));
    }
    for (var i=8; i>0; i--) {
      var diverName = $("#group2Leaderboard tbody tr:nth-child(" + i + ") td:nth-child(2)").text();
      if (diverName == 'undefined' || diverName == null || diverName == "") { continue; }
      $("#wcDiverList").append($('<option></option>').val(diverName).html(diverName));
    }
    clearDiversScores();
    break;
  // Case 1 and 2 both only work if we actually have at least 4 divers to start with.
  case 1:
    var newGroup = [];
    for (var i=0; i<group1Pairings[0].length; i++) {
      var diver1 = JSON.parse(localStorage.getItem(group1Pairings[0][i][0]));
      var diver2 = JSON.parse(localStorage.getItem(group1Pairings[0][i][1]));
      if (Number(totalDiverRoundScores(diver1)) > Number(totalDiverRoundScores(diver2))) {
        newGroup.push(diver1.name);
      }
      else {
        newGroup.push(diver2.name);
      }
    }
    group1Pairings.push([[newGroup[0], newGroup[1]], [newGroup[2], newGroup[3]]]);
    newGroup = [];
    for (var i=0; i<group2Pairings[0].length; i++) {
      var diver1 = JSON.parse(localStorage.getItem(group2Pairings[0][i][0]));
      var diver2 = JSON.parse(localStorage.getItem(group2Pairings[0][i][1]));
      if (Number(totalDiverRoundScores(diver1)) > Number(totalDiverRoundScores(diver2))) {
        newGroup.push(diver1.name);
      }
      else {
        newGroup.push(diver2.name);
      }
    }
    group2Pairings.push([[newGroup[0], newGroup[1]], [newGroup[2], newGroup[3]]]);
    reviseWCDiverList();
    clearDiversScores();
    break;
  case 2:
    var newGroup = [];
    for (var i=0; i<group1Pairings[1].length; i++) {
      var diver1 = JSON.parse(localStorage.getItem(group1Pairings[1][i][0]));
      var diver2 = JSON.parse(localStorage.getItem(group1Pairings[1][i][1]));
      if (Number(totalDiverRoundScores(diver1)) > Number(totalDiverRoundScores(diver2))) {
        newGroup.push(diver1.name);
      }
      else {
        newGroup.push(diver2.name);
      }
    }
    group1Pairings.push([[newGroup[0], newGroup[1]]]);
    newGroup = [];
    for (var i=0; i<group2Pairings[1].length; i++) {
      var diver1 = JSON.parse(localStorage.getItem(group2Pairings[1][i][0]));
      var diver2 = JSON.parse(localStorage.getItem(group2Pairings[1][i][1]));
      if (Number(totalDiverRoundScores(diver1)) > Number(totalDiverRoundScores(diver2))) {
        newGroup.push(diver1.name);
      }
      else {
        newGroup.push(diver2.name);
      }
    }
    group2Pairings.push([[newGroup[0], newGroup[1]]]);
    reviseWCDiverList();
    clearDiversScores();
    break;
  }
}

function setupWinterCup() {
  bracketRound=0;
  initializeBrackets();
  updateBracketUI();
  $("#wcRoundNum").text(currentFinalsRound);
}

/*
 * Set/show the current diver in the finals competition
 */
function showCurrentFinalsDiver() {
  var diverName = localStorage.getItem("currentFinalsDiver");
  var currentDiver = JSON.parse(localStorage.getItem(diverName));
  var currentRound = localStorage.getItem("currentFinalsRound");
  $("#wcCurrentDiverName").text(diverName);
  var dd = getDD(currentDiver["dive"+currentRound] + currentDiver["height"+currentRound]);
  $("#wcDD").val(dd);
  var dive = "[" + currentDiver["height"+currentRound] + "] " + currentDiver["dive"+currentRound] + " / DD: " + dd;
  $("#wcCurrentDiverDive").text(dive);
  var scores = currentDiver["score"+currentRound];
  if (typeof scores == 'undefined') { scores = ['','','']; }
  $("#wcScore1").val(scores[0]);
  $("#wcScore2").val(scores[1]);
  $("#wcScore3").val(scores[2]);
  var note = currentDiver["note"+currentRound];
  if (typeof note == 'undefined') { note = ""; }
  $("wcNote").val(note);
}

/*
 * This will update the bracket UI with the current diver scores. It also updates who would advance based on the score values.
 */
function updateBracketUI() {
  //group1Pairings[round][pair][diver1|diver2]
  // account for no diver / bye diver
  switch (bracketRound) {
  case 0: 
    var scores = [[],[],[]];
    for (var i=0; i<group1Pairings[0].length; i++) {
      var diver1 = JSON.parse(localStorage.getItem(group1Pairings[0][i][0]));
      var diver2 = JSON.parse(localStorage.getItem(group1Pairings[0][i][1]));
      scores[i] = [Number(totalDiverRoundScores(diver1)), Number(totalDiverRoundScores(diver2))];
    }
    group1BracketData["results"][0][bracketRound] = scores;
    scores = [[],[],[]];
    for (var i=0; i<group2Pairings[0].length; i++) {
      var diver1 = JSON.parse(localStorage.getItem(group2Pairings[0][i][0]));
      var diver2 = JSON.parse(localStorage.getItem(group2Pairings[0][i][1]));
      scores[i] = [Number(totalDiverRoundScores(diver1)), Number(totalDiverRoundScores(diver2))];
    }
    group2BracketData["results"][0][bracketRound] = scores;
    break;
  case 1:
    var scores = [[],[]];
    for (var i=0; i<group1Pairings[1].length; i++) {
      var diver1 = JSON.parse(localStorage.getItem(group1Pairings[1][i][0]));
      var diver2 = JSON.parse(localStorage.getItem(group1Pairings[1][i][1]));
      scores[i] = [Number(totalDiverRoundScores(diver1)), Number(totalDiverRoundScores(diver2))];
      group1BracketData["results"][0][bracketRound] = scores;
    }
    scores = [[],[]];
    for (var i=0; i<group2Pairings[1].length; i++) {
      var diver1 = JSON.parse(localStorage.getItem(group2Pairings[1][i][0]));
      var diver2 = JSON.parse(localStorage.getItem(group2Pairings[1][i][1]));
      scores[i] = [Number(totalDiverRoundScores(diver1)), Number(totalDiverRoundScores(diver2))];
      group2BracketData["results"][0][bracketRound] = scores;
    }
    break;
  case 2:
    var scores = [[]];
    for (var i=0; i<group1Pairings[2].length; i++) {
      var diver1 = JSON.parse(localStorage.getItem(group1Pairings[2][i][0]));
      var diver2 = JSON.parse(localStorage.getItem(group1Pairings[2][i][1]));
      scores[i] = [Number(totalDiverRoundScores(diver1)), Number(totalDiverRoundScores(diver2))];
      group1BracketData["results"][0][bracketRound] = scores;
    }
    scores = [[]];
    for (var i=0; i<group2Pairings[2].length; i++) {
      var diver1 = JSON.parse(localStorage.getItem(group2Pairings[2][i][0]));
      var diver2 = JSON.parse(localStorage.getItem(group2Pairings[2][i][1]));
      scores[i] = [Number(totalDiverRoundScores(diver1)), Number(totalDiverRoundScores(diver2))];
      group2BracketData["results"][0][bracketRound] = scores;
    }
    break;
  }
  $('#group1Bracket').bracket({
    skipConsolationRound: true,
    init: group1BracketData
    });
  $('#group2Bracket').bracket({
    dir: 'rl',
    skipConsolationRound: true,
    init: group2BracketData
  });
}