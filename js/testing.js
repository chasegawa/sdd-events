/*
 * Utility function to clear the registered divers' scores
 */
function clearDiverScores(diver) {
  diver.score1 = ["","",""];
  diver.score2 = ["","",""];
  diver.score3 = ["","",""];
  diver.score4 = ["","",""];
  diver.score5 = ["","",""];
  diver.roundScores = [0.0, 0.0, 0.0, 0.0, 0.0];
  diver.total = 0.0;
  // Put the diver detail into local storage keyed by name
  localStorage.setItem(diver.name, JSON.stringify(diver));
}